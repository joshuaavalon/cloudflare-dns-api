# @joshuaavalon/cloudflare-dns-api

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![Coverage][coverage_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Cloudflare DDNS api wrapper.

## Installation

```
npm i @joshuaavalon/cloudflare-dns-api
```

[license]: https://gitlab.com/joshuaavalon/cloudflare-dns-api/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/joshuaavalon/cloudflare-dns-api/pipelines
[pipelines_badge]: https://gitlab.com/joshuaavalon/cloudflare-dns-api/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/joshuaavalon/cloudflare-dns-api/badges/master/coverage.svg
[npm]: https://www.npmjs.com/package/@joshuaavalon/cloudflare-dns-api
[npm_badge]: https://img.shields.io/npm/v/@joshuaavalon/cloudflare-dns-api/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg

## [1.1.6](https://gitlab.com/joshuaavalon/cloudflare-dns-api/compare/v1.1.5...v1.1.6) (2021-06-20)


### Bug Fixes

* update dependencies ([e0e8b83](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/e0e8b83e28114be42c00e38605a0758ea7b0fe1e))

## [1.1.5](https://gitlab.com/joshuaavalon/cloudflare-dns-api/compare/v1.1.4...v1.1.5) (2020-11-30)


### Bug Fixes

* missing test ([466a510](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/466a5101affc33f4d21d565ffc43cbaa2fff719d))
* update dependencies ([75779b8](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/75779b8d6b31eaf3600f9d7bf029b16518a3ee94))

## [1.1.4](https://gitlab.com/joshuaavalon/cloudflare-dns-api/compare/v1.1.3...v1.1.4) (2020-09-30)


### Bug Fixes

* update @joshuaavalon/eslint-config-typescript ([e753ec7](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/e753ec78e5f5f47db51df964ac8b632eb812fee5))
* update dependencies ([2713b75](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/2713b756b530fc0d8b9ac44bf5cb9f9169164fe7))

## [1.1.3](https://gitlab.com/joshuaavalon/cloudflare-dns-api/compare/v1.1.2...v1.1.3) (2020-09-30)


### Bug Fixes

* POST not working ([1805b91](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/1805b91d2210684d85e0493a360be4908e9479e4))

## [1.1.2](https://gitlab.com/joshuaavalon/cloudflare-dns-api/compare/v1.1.1...v1.1.2) (2020-08-21)


### Bug Fixes

* missing create dns export ([646a93f](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/646a93fb2fbf34f94bbe313cab7be3cc7adc107e))

## [1.1.1](https://gitlab.com/joshuaavalon/cloudflare-dns-api/compare/v1.1.0...v1.1.1) (2020-08-21)


### Bug Fixes

* listing api result ([a038bee](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/a038bee5449ad701a80f6b7bc147e3590ce9a4fe))

# [1.1.0](https://gitlab.com/joshuaavalon/cloudflare-dns-api/compare/v1.0.1...v1.1.0) (2020-08-21)


### Features

* update list zone parameters ([e772c9f](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/e772c9f458f4283f096cb00eeee23b918fa022cf))

## [1.0.1](https://gitlab.com/joshuaavalon/cloudflare-dns-api/compare/v1.0.0...v1.0.1) (2020-08-17)


### Bug Fixes

* update dependencies ([c7b74c3](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/c7b74c3e4b0c6e5cecdf3ae71b90a5f958503626))

# 1.0.0 (2020-07-13)


### Features

* initial commit ([2bdae26](https://gitlab.com/joshuaavalon/cloudflare-dns-api/commit/2bdae26c54f87a2c77a1dae06164f76ec243521e))
